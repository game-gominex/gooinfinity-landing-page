import React, {Component} from 'react';
import cls from './App.module.scss';
import classnames from 'classnames';
import img1 from './assets/images/team.svg';
import sky from './assets/images/skype.png';
import whatsapp from './assets/images/whatsapp.png';
import linkedin from './assets/images/linkedin.png';
import Carousel from 'nuka-carousel';
import art1 from './assets/images/schola.jpg';
import art2 from './assets/images/4ufreegames.jpg';
import art3 from './assets/images/Hikids.jpg';
import art4 from './assets/images/ezbook.jpg';
import art5 from './assets/images/gominex.jpg';
import infinity from './assets/images/infinity.png';
import culture from './assets/images/culture.png';
import activity1 from './assets/images/activity1.png';
import activity2 from './assets/images/activity2.png';
import activity3 from './assets/images/activity3.png';
import logo from './assets/images/logo2.svg';
// import InViewMonitor from 'react-inview-monitor';
import {IntlProvider, FormattedMessage} from 'react-intl';
import messages from "./config/messages";
import ReactFlagsSelect from 'react-flags-select';


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isMenuOpen: false,
            outProductActive: false,
            aboutUsActive: false,
        };
    }

    onSelectFlag = (lang) => {
        if (lang && lang !== localStorage.lang) {
            localStorage.setItem('lang', lang);
            window.location.reload();
        }
    };

    render() {

        let lang = localStorage.lang || 'VN';

        let locale = 'vi';
        if (lang === 'GB') {
            locale = 'en'
        }


        return (
            <IntlProvider
                locale={locale}
                messages={messages[lang]}
                key={lang}
            >
                <div className={cls.wrapper}>
                    <section className={cls.intro}>
                        <header className={cls.header}>
                            <div className={classnames(cls.navPanel, {
                                [cls.menuOpen]: this.state.isMenuOpen
                            })}>
                                <div className={cls.closeBtn}>
                                    <i onClick={() => this.setState((prevState) => ({
                                        isMenuOpen: !prevState.isMenuOpen
                                    }))} className="fas fa-times"/>
                                </div>
                                <ul>
                                    <li className={classnames({
                                        [cls.active]: this.state.outProductActive
                                    })}>
                                        <a href={'#product'} onClick={() => {
                                            this.setState({
                                                outProductActive: true,
                                                aboutUsActive: false,
                                                isMenuOpen: false
                                            })
                                        }}>
                                            <FormattedMessage id={'header.our_product'}/>
                                        </a>
                                    </li>
                                    <li className={classnames({
                                        [cls.active]: this.state.aboutUsActive
                                    })}>
                                        <a href={'#culture'} onClick={() => {
                                            this.setState({
                                                outProductActive: false,
                                                aboutUsActive: true,
                                                isMenuOpen: false
                                            })
                                        }}>
                                            <FormattedMessage id={'header.about'}/>
                                        </a>
                                    </li>
                                    <li style={{marginTop: '2rem'}}>
                                        <ReactFlagsSelect
                                            countries={["VN", "GB"]}
                                            customLabels={{
                                                "VN": "Viet Nam",
                                                "GB": "English",
                                            }}
                                            defaultCountry={lang}
                                            placeholder="Select Language"
                                            onSelect={this.onSelectFlag}
                                        />
                                    </li>
                                </ul>
                            </div>
                            <div className={cls.headerContainer}>
                                <div className={cls.leftHeader}>
                                    <div className={cls.logo}>
                                        <a href={'/'}>
                                            <img src={logo}/>
                                        </a>
                                    </div>
                                </div>
                                <div className={cls.rightHeader}>
                                    <ul>
                                        <li className={classnames({
                                            [cls.active]: this.state.outProductActive
                                        })}>
                                            <a href={'#product'} onClick={() => {
                                                this.setState({
                                                    outProductActive: true,
                                                    aboutUsActive: false
                                                })
                                            }}>
                                                <FormattedMessage id={'header.our_product'}/>
                                            </a>
                                        </li>
                                        <li className={classnames({
                                            [cls.active]: this.state.aboutUsActive
                                        })}>
                                            <a href={'#culture'} onClick={() => {
                                                this.setState({
                                                    outProductActive: false,
                                                    aboutUsActive: true
                                                })
                                            }}>
                                                <FormattedMessage id={'header.about'}/>
                                            </a>
                                        </li>
                                        <li>
                                            <ReactFlagsSelect
                                                countries={["VN", "GB"]}
                                                customLabels={{
                                                    "VN": "Viet Nam",
                                                    "GB": "English",
                                                }}
                                                defaultCountry={lang}
                                                placeholder="Select Language"
                                                onSelect={this.onSelectFlag}
                                            />
                                        </li>
                                    </ul>
                                    <div className={cls.headerToggle}>
                                        <i onClick={() => this.setState((prevState) => ({
                                            isMenuOpen: !prevState.isMenuOpen
                                        }))} className="fas fa-bars"/>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div className={cls.introContent}>
                            <div className={cls.introLeft}>
                                <p>
                                    <FormattedMessage id={'intro.achieving'}/>
                                    <br/>
                                    <FormattedMessage id={'intro.with'}/>
                                    <img src={img1}/>
                                </p>
                                <p>
                                    <FormattedMessage id={'intro.we_provide'}/>
                                </p>
                                <button>
                                    <a href={'#footer'}>
                                        <FormattedMessage id={'intro.contact'}/>
                                    </a>
                                </button>

                                <div className={cls.analytic}>
                                    <div className={cls.analyticItem}>
                                        <p>20+</p>
                                        <span>START UP</span>
                                    </div>
                                    <div className={cls.analyticItem}>
                                        <p>30+</p>
                                        <span>WEB APP</span>
                                    </div>
                                    <div className={cls.analyticItem}>
                                        <p>30+</p>
                                        <span>MOBILE APP</span>
                                    </div>
                                </div>
                            </div>
                            <div className={cls.introRight}>
                                <div className='banner'>
                                    <ul className='slider'>
                                        <li/>
                                        <li/>
                                        <li/>
                                    </ul>
                                </div>
                                <div className={cls.social}>
                                    <img src={whatsapp}/>
                                    <img src={sky}/>
                                    <img src={linkedin}/>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className={cls.products} id={'product'}>
                        <div className={cls.carousel}>
                            <Carousel
                                autoplay
                                wrapAround
                                renderCenterLeftControls={({previousSlide}) => (
                                    <i style={{
                                        fontSize: '2rem',
                                        marginLeft: '1.2rem',
                                        cursor: 'pointer',
                                        color: 'white'
                                    }}
                                       onClick={previousSlide}
                                       className="fas fa-chevron-left"/>
                                )}
                                renderCenterRightControls={({nextSlide}) => (
                                    <i style={{
                                        fontSize: '2rem',
                                        marginRight: '1.2rem',
                                        cursor: 'pointer',
                                        color: 'white'
                                    }}
                                       onClick={nextSlide}
                                       className="fas fa-chevron-right"/>
                                )}
                            >
                                <div className={cls.carouselItem}>
                                    <img src={art5}/>
                                    <div className={cls.textContent}>
                                        <h3>Gominex</h3>
                                        <span>
                                            <FormattedMessage id='product.help_you_make'/>
                                        </span>
                                        <br/>
                                        <button>
                                            <a href='https://gominex.com/' target='blank'>
                                                <FormattedMessage id={'btn.visit_website'}/>
                                            </a>
                                        </button>
                                    </div>
                                </div>
                                <div className={cls.carouselItem}>
                                    <img src={art1}/>
                                    <div className={cls.textContent}>
                                        <h3>Schola</h3>
                                        <span>
                                        <FormattedMessage id={'product.american'}/>
                                    </span>
                                        <br/>
                                        <button>
                                            <FormattedMessage id={'btn.coming_soon'}/>
                                        </button>
                                    </div>
                                </div>
                                <div className={cls.carouselItem}>
                                    <img src={art2}/>
                                    <div className={cls.textContent}>
                                        <h3>4UFreegames</h3>
                                        <span>
                                        <FormattedMessage id={'product.best_online_game'}/>
                                    </span>
                                        <button>
                                            <a href='https://www.4ufreegames.com/'
                                               target='blank'>
                                                <FormattedMessage id={'btn.visit_website'}/>
                                            </a>
                                        </button>
                                    </div>
                                </div>
                                <div className={cls.carouselItem}>
                                    <img src={art3}/>
                                    <div className={cls.textContent}>
                                        <h3>HiKid</h3>
                                        <span>
                                        <FormattedMessage id={'product.a_mobile_application'}/>
                                    </span>
                                        <br/>
                                        <button>
                                            <FormattedMessage id={'btn.coming_soon'}/>
                                        </button>
                                    </div>
                                </div>
                                <div className={cls.carouselItem}>
                                    <img src={art4}/>
                                    <div className={cls.textContent}>
                                        <h3>Ezbook</h3>
                                        <span>
                                        <FormattedMessage id={'product.get_all'}/>
                                    </span>
                                        <br/>
                                        <button>
                                            <a href='https://play.google.com/store/apps/details?id=com.gox.audiobook'
                                               target='blank'>
                                                <FormattedMessage id={'btn.available_google'}/>
                                            </a>
                                        </button>
                                    </div>
                                </div>
                            </Carousel>
                        </div>
                    </section>
                    <section className={cls.culture} id='culture'>
                        <div className={cls.titleCulture}>
                            <img src={infinity}/>
                            <img src={culture}/>
                        </div>
                        <div className={cls.cardCulture}>
                            <div className={cls.cardItem}>
                                <div className={cls.imageWrapper} style={{
                                    backgroundImage: `url("${activity1}")`,
                                }}/>
                                <h3><FormattedMessage id={'culture.transparent'}/></h3>
                                <span>
                                    <FormattedMessage id={'culture.flat'}/>
                                </span>
                            </div>
                            <div className={cls.cardItem}>
                                <div className={cls.imageWrapper} style={{
                                    backgroundImage: `url("${activity2}")`,
                                }}/>
                                <h3><FormattedMessage id={'culture.creative'}/></h3>
                                <span>
                                    <FormattedMessage id={'culture.Goofinity'}/>
                                </span>
                            </div>
                            <div className={cls.cardItem}>
                                <div className={cls.imageWrapper} style={{
                                    backgroundImage: `url("${activity3}")`,
                                }}/>
                                <h3><FormattedMessage id={'culture.flexible'}/></h3>
                                <span>
                                    <FormattedMessage id={'culture.build_your'}/>
                                </span>
                            </div>
                        </div>
                    </section>
                    <section className={cls.footer} id={'footer'}>
                        <div className={cls.footerContainer}>
                            <div className={cls.leftFooter}>
                                <a href={'/'}>
                                    <img src={logo} style={{width: '7.5rem'}}/>
                                </a>
                            </div>
                            <div className={cls.centerFooter}>
                                <ul>
                                    <li>
                                        <a>
                                            Our Product
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            Team
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            Career
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            About Us
                                        </a>
                                    </li>
                                </ul>
                                <div className={cls.textBottom}>
                                    <p>
                                        Email : contact@goofinity.com | Phone: (+84) 932 500 788
                                    </p>
                                    <p>
                                        Address: 70 Phan Thanh, Da Nang, Viet Nam
                                    </p>
                                </div>
                            </div>
                            {/*<div className={cls.rightFooter}>*/}
                            {/*    <p>ffdfdfd</p>*/}
                            {/*</div>*/}
                        </div>
                    </section>
                </div>
            </IntlProvider>
        );
    }

}

export default App;
