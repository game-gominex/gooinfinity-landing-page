import React from "react";

export default {
    VN: {
        'header.our_product': 'Sản phẩm',
        'header.about': 'Về chúng tôi',
        'intro.achieving': 'Công việc sẽ hiệu quả hơn',
        'intro.with': 'với',
        'intro.we_provide': 'Chúng tôi cung cấp dịch vụ và giải pháp giúp bạn có một sản phẩm tốt và hiệu quả.',
        'intro.contact': 'Liên hệ với chúng tôi ngay!',
        'product.help_you_make': 'Giúp bạn kiếm lợi nhuận ngay cả khi bạn bận rộn. Không còn những đêm mất ngủ.',
        'product.american': 'Trường quốc tế Mỹ tại nhà, có 5-12 năm kinh nghiệm giáo trình, các hoạt động và tạo sự tự tin khi giao tiếp Tiếng Anh.',
        'product.best_online_game': 'Website trò chơi trực trực tuyến mang đến cho người chơi những trải nghiệm thú vị.',
        'product.a_mobile_application': 'Một ứng dụng di động cung cấp hơn 400 trò chơi thú vị và mang tính giáo dục cho các lớp từ Prek đến 6!',
        'product.get_all': 'Tủ sách online cho người đọc mọi lúc mọi nơi!',
        'btn.visit_website': 'Chi tiết',
        'btn.coming_soon': 'Sắp ra mắt',
        'btn.available_google': 'Google Play',
        'culture.transparent': 'Rõ ràng',
        'culture.creative': 'Sáng tạo',
        'culture.flexible': 'Linh hoạt',
        'culture.build_your': 'Xây dựng lịch trình, không gian làm việc của riêng bạn. Hãy là phiên bản tốt nhất của chính mình!',
        'culture.Goofinity': 'Môi trường cho phép mọi người phát huy tối đa tiềm năng và tài năng, bất kể đó là gì.',
        'culture.flat': 'Tổ chức phẳng, có thể giao tiếp với tất cả nhà quản lý và lãnh đạo, tự do tranh luận trong các cuộc họp hàng tuần.'
    },
    GB: {
        'header.our_product': 'Our Product',
        'header.about': 'About Us',
        'intro.achieving': 'Achieving effective work',
        'intro.with': 'with',
        'intro.we_provide': 'We provide services and solutions that help you have a good and effective product.',
        'intro.contact': 'Contact Us Now !',
        'product.help_you_make': 'Help you make profits even when you\'re busy at work. No more sleepless nights.',
        'product.american': 'American International School At Home. Ages 5-12 years experience American syllabus, activities, and English speaking confidence.',
        'product.best_online_game': 'The best online games are constanly updated, wishing everyone a fun experience.',
        'product.a_mobile_application': 'A mobile application provides over 400 fun and educational games for grades PreK through 6!',
        'product.get_all': 'Get all the free books or Audio books you love, anywhere you go!',
        'btn.visit_website': 'Visit website',
        'btn.coming_soon': 'Coming soon',
        'btn.available_google': 'Available on Google Play',
        'culture.transparent': 'transparent',
        'culture.creative': 'creative',
        'culture.flexible': 'flexible',
        'culture.build_your': 'Build your own schedule. Choose your own workspace. Be the best of yourself!',
        'culture.Goofinity': 'Goofinity environment enables everyone to maximize potentials and talents, whatever it is.',
        'culture.flat': 'Flat organization. All managers and leaders are always accessible. Weekly all-hand meetings are when you can freely ask anything.'
    }
}
